const proggresBarToHtml = (percentage, color) => {
  const progress = document.createElement("div");
  progress.className = "progress";

  const progressBar = document.createElement("div");
  progressBar.className = "progress-bar";
  progressBar.style.width = `${percentage}%`;
  progressBar.style.backgroundColor = color;

  const progressBarLabel = document.createElement("span");
  progressBarLabel.className = "progress-bar_label";
  progressBarLabel.innerHTML = `${percentage}%`;

  progressBar.appendChild(progressBarLabel);
  progress.appendChild(progressBar);
  return progress;
};

const renderProgressBar = (percentage) => {
  const container = document.getElementById("container");
  const progress1 = proggresBarToHtml(percentage, "red");
  const progress2 = proggresBarToHtml(percentage, "blue");
  const progress3 = proggresBarToHtml(percentage, "green");
  const progress4 = proggresBarToHtml(percentage, "yellow");
  const progress5 = proggresBarToHtml(percentage, "purple");
  container.appendChild(progress1);
  container.appendChild(progress2);
  container.appendChild(progress3);
  container.appendChild(progress4);
  container.appendChild(progress5);
};

renderProgressBar(40);

const btnOnClick = () => {
  document.querySelectorAll(".progress-bar").forEach((item) => {
    item.style.width = "100%";
    item.querySelector(".progress-bar_label").innerHTML = "100%";
  });
};
